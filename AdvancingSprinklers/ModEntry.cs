using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley;

namespace AdvancingSprinklers
{
    /// <summary>The mod entry point.</summary>
    public class ModEntry : Mod
    {
        /*********
        ** Public methods
        *********/
        /// <summary>The mod entry point, called after the mod is first loaded.</summary>
        /// <param name="helper">Provides simplified APIs for writing mods.</param>
        public override void Entry(IModHelper helper)
        {
            ChangeSprinklerRecipes();
            AddDebugCode(helper);
        }

        [ConditionalAttribute("DEBUG")]
        private void AddDebugCode(IModHelper helper)
        {
            helper.ConsoleCommands.Add("test_sprinklers", "Gives the player recipes and resources to craft sprinklers. (Cheat for testing)\n\nUsage: test_sprinklers", this.GiveRecipesAndItems);
        }

        /// <summary>The method that changes the sprinkler recipes</summary>
        private void ChangeSprinklerRecipes()
        {
            var recipes = CraftingRecipe.craftingRecipes;
            recipes["Quality Sprinkler"] = "599 1 336 1 338 1/Home/621/false/Farming 6";
            recipes["Iridium Sprinkler"] = "621 1 337 1 787 1/Home/645/false/Farming 9";
        }
        
        /// <summary>The method to give the player recipes and itmes needed to test the new recipes.</summary>
        private void GiveRecipesAndItems(string command, string[] args)
        {
            Game1.player.craftingRecipes.Add("Quality Sprinkler", 0);
            Game1.player.craftingRecipes.Add("Iridium Sprinkler", 0);
            Game1.player.addItemToInventory(new StardewValley.Object(599, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(336, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(645, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(335, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(334, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(338, 10));
            Game1.player.addItemToInventory(new StardewValley.Object(787, 10));
        }
    }
}